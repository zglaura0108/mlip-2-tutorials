#!/bin/bash

export OMP_NUM_THREADS=1

rm -f ElasticConstants.txt

cd ElConst

../../../mlp convert-cfg relaxed.cfg input.pos --output-format=lammps-datafile

../../../lmp -in in.elastic -log none > ElasticConstants.txt
rm -f restart.equil

python read-ElasticConstants-create-file.py

cat ElasticConstants.txt >> ../ElasticConstants.txt

cd ../


