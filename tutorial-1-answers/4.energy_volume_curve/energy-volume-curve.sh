#!/bin/bash

export OMP_NUM_THREADS=1

../mlp compress-extend relaxed.cfg deformed.cfg
../mlp calc-efs pot.mtp deformed.cfg deformed_efs.cfg
python cfg-to-energy-volume.py deformed_efs.cfg E_V.txt
gnuplot plot.p
